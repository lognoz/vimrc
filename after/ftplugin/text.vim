" Document width
setlocal textwidth=74

" Replace tabs by spaces
setlocal tabstop=2
setlocal shiftwidth=2
setlocal expandtab
