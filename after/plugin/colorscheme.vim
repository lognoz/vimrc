" ============================================================================
" File:        colorscheme.vim
" Author:      Marc-Antoine Loignon <info@lognoz.com>
" Licence:     Vim licence
" Website:     https://www.gitlab.com/lognoz/vimrc
" Version:     1.1
"
"  Permission is hereby granted to use and distribute this code, with or
"  without modifications, provided that this copyright notice is copied
"  with it. Like anything else that's free, this vimrc is provided *as is*
"  and comes with no warranty of any kind, either expressed or implied. In
"  no event will the copyright holder be liable for any damamges resulting
"  from the use of this software.
" ============================================================================

colorscheme onedark

highlight Normal        ctermbg=0
highlight LineNr        ctermfg=239
highlight NonText       ctermfg=234
highlight CursorLine    ctermbg=233
highlight SpecialKey    ctermfg=234
highlight Pmenu         ctermbg=232   ctermfg=239
highlight PmenuSel      ctermfg=250   ctermbg=232
highlight Whitespace    ctermbg=11
highlight StatusLineNC  ctermbg=233
highlight Search        ctermbg=none  ctermfg=none  cterm=bold,underline
highlight IncSearch     ctermbg=none  ctermfg=none  cterm=bold,underline
